import os

import googleapiclient.discovery
import googleapiclient.errors
import google_auth_oauthlib.flow

# Disable OAuthlib's HTTPS verification when running locally.
# *DO NOT* leave this option enabled in production.
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]

api_service_name = "youtube"
api_version = "v3"
DEVELOPER_KEY = ""
client_secret_file = "client_secret.json"


def putIntoPlaylist(videoID, creds, playlistId):
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=creds)

    request = youtube.playlistItems().insert(
        part="snippet",
        body={
            "snippet": {
                "playlistId": playlistId,
                "resourceId": {
                    "kind": "youtube#video",
                    "videoId": videoID
                }
            }
        }
    )
    response = request.execute()
    print("written")


def getVideos(searchString):
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY)

    request = youtube.search().list(
        part="snippet",
        maxResults=25,
        q=searchString
    )
    response = request.execute()

    print(response["items"][0]["snippet"]["title"])
    return response["items"][0]["id"]["videoId"]


def loadFile(fileName):
    file = open(fileName).read()
    print(file.split("\n"))
    return file.split("\n")


def googleAuth():
    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secret_file, scopes)
    credentials = flow.run_console()
    return credentials


def main():
    auto = googleAuth()
    playlist = input("Paste your playlist ID: ")
    for line in loadFile("pisnicky.txt"):
        video = getVideos(line)
        print(video)
        putIntoPlaylist(video, auto, playlist)


if __name__ == '__main__':
    main()
