from main import googleAuth
import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]

def getShoot(playlist, auth):
    api_service_name = "youtube"
    api_version = "v3"

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=auth)

    request = youtube.playlistItems().list(
        part="snippet,contentDetails",
        maxResults=50,
        playlistId=playlist
    )
    response = request.execute()

    print(response)
    return response

def main():
    playlistID = input("Paste playlist ID")
    videa = getShoot(playlistID, googleAuth())
    output = ""
    for item in videa["items"]:
        print(item["snippet"]["title"])
        output += item["snippet"]["title"]+"\n"

    sobor = open("out.txt", "w", encoding="utf-8")
    sobor.write(output)


if __name__ == "__main__":
    main()
