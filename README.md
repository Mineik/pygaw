# Youtube playlists getter, writer

## Prerequisites
* You get your app credentials for google API with youtube access
* You got your secrets and developer key
* Paste your developer key on line 15 in `main.py`
* You have installed dependencies from `requirements.txt`
* You put your secrets file in root as `client_secret.json`
* You put names of videos you wanna add to playlist to `pisnicky.txt`

## Usage
### Updating youtube playlist
* Check you have done all in (Prerequisities)[#Prerequisities]
* run `python main.py`
* authorize with google, paste the token when you're prompted
* Get your playlist id (from for example link in your webpage)
* Paste it when you get prompted
